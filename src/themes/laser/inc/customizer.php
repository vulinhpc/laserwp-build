<?php
/**
 * Laser Theme Customizer
 *
 * @package Laser
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function laser_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'laser_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'laser_customize_partial_blogdescription',
		) );
	}

	// Disable Font Awesome setting
	$wp_customize->add_setting( 'disable_font_awesome' , array(
	    'default' => false,
	    'sanitize_callback' => 'absint',
	) );

	 $wp_customize->add_control('disable_font_awesome', array(
        'setting' => 'disable_font_awesome',
        'label'    => __('Disable Font Awesome'),
        'description'    => __('Incase you dont need FontAwesome, this setting will remove FontAwesome css, so your website will load faster'),
        'section'  => 'colors',
        'type'     => 'checkbox',
    ));

	 // Disable Swiper slider library setting 
	$wp_customize->add_setting( 'banner_type' , array(
	    'default' => 'swiper',
	    // 'sanitize_callback' => 'absint',
	) );

	 $wp_customize->add_control('banner_type', array(
			'setting'          => 'banner_type',
			'label'            => __('Banner Setting'),
			'description'      => __('Select your banner setting'),
			'section'          => 'colors',
			'type'             => 'radio',
			'choices'          => array(
				'image-background' => 'Image Background',
				'fullpage-video'   => 'Full Page Video',
				'swiper-banner'           => 'Swiper Images Slider',
				'disabled'         => 'Disable Banner Section'
	        ),
    ));

}
add_action( 'customize_register', 'laser_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function laser_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function laser_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function laser_customize_preview_js() {
	wp_enqueue_script( 'laser-customizer', LASER_THEME_URI . '/assets/js/customizer/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'laser_customize_preview_js' );
