<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Laser
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function laser_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'laser_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function laser_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'laser_pingback_header' );

/**
 * Check if FontAwesome is disabled using customizer
 *
 * @return     boolean:  true if FontAwesome is disabled
 */
function laser_is_disable_font_awesome()
{

	return get_theme_mod('disable_font_awesome', false);

}


/**
 * Get banner type
 *
 * @return     string 
 */
function laser_get_banner_type()
{

	return get_theme_mod('banner_type', 'swiper');

}


function laser_banner_section()
{
	$banner_type = laser_get_banner_type();

	if ( $banner_type !== 'disabled') {
	
		get_template_part('partials/' . $banner_type);

	}

}

/**
 * Create back to top button when scroll
 */
function laser_back_to_top()
{

	if ( ! laser_is_disable_font_awesome() ) {

		$back_to_top_icon = get_theme_mod('laser_back_to_top_icon', 'fa fa-arrow-up');
		$icon = '<i class="' . $back_to_top_icon .'" aria-hidden="true"></i>';
		
	}else{
		$icon = '&uarr;';
	}

	echo '<!-- Back to top -->';
	echo '<a href="#back-to-top" id="back-to-top" title="' .__('Back to top', 'laser') . '">' . $icon .'</a>';
	
}


/**
 * Laser Minify css
 *
 * @param      string  $css    The css
 *
 * @return     string  css minified
 */
function laser_minify_css( $css = '' )
{
	
	// Return if no CSS
	if ( ! $css ) return;

	// Normalize whitespace
	$css = preg_replace( '/\s+/', ' ', $css );

	// Remove ; before }
	$css = preg_replace( '/;(?=\s*})/', '', $css );

	// Remove space after , : ; { } */ >
	$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );

	// Remove space before , ; { }
	$css = preg_replace( '/ (,|;|\{|})/', '$1', $css );

	// Strips leading 0 on decimal values (converts 0.5px into .5px)
	$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );

	// Strips units if value is 0 (converts 0px to 0)
	$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );

	// Trim
	$css = trim( $css );

	// Return minified CSS
	return $css;
}

