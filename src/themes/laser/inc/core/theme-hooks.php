<?php
/**
 * Theme Hook 
 *
 * @package  Laser
 * @since    1.0
 */
/**
 * Themes and Plugins can check for laser_hooks using current_theme_supports( 'laser_hooks', $hook )
 * to determine whether a theme declares itself to support this specific hook type.
 *
 * Example:
 * <code>
 * 		// Declare support for all hook types
 * 		add_theme_support( 'laser_hooks', array( 'all' ) );
 *
 * 		// Declare support for certain hook types only
 * 		add_theme_support( 'laser_hooks', array( 'header', 'content', 'footer' ) );
 * </code>
 */
add_theme_support( 'laser_hooks', array(

	/**
	 * As a Theme developer, use the 'all' parameter, to declare support for all
	 * hook types.
	 * Please make sure you then actually reference all the hooks in this file,
	 * Plugin developers depend on it!
	 */
	'all',

	/**
	 * Themes can also choose to only support certain hook types.
	 * Please make sure you then actually reference all the hooks in this type
	 * family.
	 *
	 * When the 'all' parameter was set, specific hook types do not need to be
	 * added explicitly.
	 */
	'html',
	'body',
	'head',
	'header',
	'content',
	'entry',
	'comments',
	'sidebars',
	'sidebar',
	'footer',

	/**
	 * If/when WordPress Core implements similar methodology, Themes and Plugins
	 * will be able to check whether the version of THA supplied by the theme
	 * supports Core hooks.
	 */
	//'core',
) );

/**
 * Determines, whether the specific hook type is actually supported.
 *
 * Plugin developers should always check for the support of a <strong>specific</strong>
 * hook type before hooking a callback function to a hook of this type.
 *
 * Example:
 * <code>
 * 		if ( current_theme_supports( 'laser_hooks', 'header' ) )
 * 	  		add_action( 'laser_head_top', 'prefix_header_top' );
 * </code>
 *
 * @param bool $bool true
 * @param array $args The hook type being checked
 * @param array $registered All registered hook types
 *
 * @return bool
 */
function laser_current_theme_supports( $bool, $args, $registered ) {
	return in_array( $args[0], $registered[0] ) || in_array( 'all', $registered[0] );
}
add_filter( 'current_theme_supports-laser_hooks', 'laser_current_theme_supports', 10, 3 );

/**
 * HTML <html> hook
 * Special case, useful for <DOCTYPE>, etc.
 * $laser_supports[] = 'html;
 */
function laser_html_before() {
	do_action( 'laser_html_before' );
}
/**
 * HTML <body> hooks
 * $laser_supports[] = 'body';
 */
function laser_body_top() {
	do_action( 'laser_body_top' );
}

function laser_body_bottom() {
	do_action( 'laser_body_bottom' );
}

/**
 * HTML <head> hooks
 *
 * $laser_supports[] = 'head';
 */
function laser_head_top() {
	do_action( 'laser_head_top' );
}

function laser_head_bottom() {
	do_action( 'laser_head_bottom' );
}

/**
 * Semantic <header> hooks
 *
 * $laser_supports[] = 'header';
 */
function laser_header_before() {
	do_action( 'laser_header_before' );
}

function laser_header_after() {
	do_action( 'laser_header_after' );
}
/**
 * Site Header
 */
function laser_header() {
	do_action( 'laser_header' );
}

function laser_header_top() {
	do_action( 'laser_header_top' );
}

function laser_header_bottom() {
	do_action( 'laser_header_bottom' );
}

/**
 * Site Banner
 */
function laser_banner() {
	do_action( 'laser_banner' );
}

function laser_banner_before() {
	do_action( 'laser_banner_top' );
}

function laser_banner_after() {
	do_action( 'laser_banner_bottom' );
}

/**
 * Semantic <content> hooks
 *
 * $laser_supports[] = 'content';
 */
function laser_content_before() {
	do_action( 'laser_content_before' );
}

function laser_content_after() {
	do_action( 'laser_content_after' );
}

function laser_content_top() {
	do_action( 'laser_content_top' );
}

function laser_content_bottom() {
	do_action( 'laser_content_bottom' );
}

function laser_content_while_before() {
	do_action( 'laser_content_while_before' );
}

function laser_content_while_after() {
	do_action( 'laser_content_while_after' );
}

/**
 * Semantic <entry> hooks
 *
 * $laser_supports[] = 'entry';
 */
function laser_entry_before() {
	do_action( 'laser_entry_before' );
}

function laser_entry_after() {
	do_action( 'laser_entry_after' );
}

function laser_entry_content_before() {
	do_action( 'laser_entry_content_before' );
}

function laser_entry_content_after() {
	do_action( 'laser_entry_content_after' );
}

function laser_entry_top() {
	do_action( 'laser_entry_top' );
}

function laser_entry_bottom() {
	do_action( 'laser_entry_bottom' );
}

/**
 * Comments block hooks
 *
 * $laser_supports[] = 'comments';
 */
function laser_comments_before() {
	do_action( 'laser_comments_before' );
}

function laser_comments_after() {
	do_action( 'laser_comments_after' );
}

/**
 * Semantic <sidebar> hooks
 *
 * $laser_supports[] = 'sidebar';
 */
function laser_sidebars_before() {
	do_action( 'laser_sidebars_before' );
}

function laser_sidebars_after() {
	do_action( 'laser_sidebars_after' );
}

function laser_sidebar_top() {
	do_action( 'laser_sidebar_top' );
}

function laser_sidebar_bottom() {
	do_action( 'laser_sidebar_bottom' );
}

/**
 * Semantic <footer> hooks
 *
 * $laser_supports[] = 'footer';
 */
function laser_footer_before() {
	do_action( 'laser_footer_before' );
}

function laser_footer_after() {
	do_action( 'laser_footer_after' );
}

function laser_footer_top() {
	do_action( 'laser_footer_top' );
}

function laser_footer_bottom() {
	do_action( 'laser_footer_bottom' );
}
