<?php
/**
 * Laser theme setup class
 * Generate and intance after theme setup
 *
 *
 * @package     Laser
 * @author      LaserWP
 * @since 1.0.0
 */

/**
 * Laser_Theme_Setup initial setup
 *
 */
if ( ! class_exists( 'Laser_Theme_Setup' ) ) {

	/**
	 * Laser_Theme_Setup initial setup
	 */
	class Laser_Theme_Setup {

		/**
		 * Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Initiator
		 *
		 * @since 1.0.0
		 * @return object
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			add_action( 'after_setup_theme', array( $this, 'setup_theme' ), 2 );
    	    add_action( 'widgets_init', array( $this, 'register_sidebars' ) );
	        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 95 );
			add_action( 'wp_head', array( $this, 'laser_custom_css' ), 9999 );
		}

		/**
		 * Setup theme
		 *
		 * @since 1.0.0
		 */
		function setup_theme() {

			/**
			 * Content Width
			 */
			if ( ! isset( $content_width ) ) {
				$content_width = apply_filters( 'laser_content_width', 780 );
			}

			/**
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on Next, use a find and replace
			 * to change 'laser' to the name of your theme in all the template files.
			 */
			load_theme_textdomain( 'laser', LASER_THEME_DIR . '/languages' );

			/**
			 * Theme Support
			 */

			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );

			// Let WordPress manage the document title.
			add_theme_support( 'title-tag' );

			// Enable support for Post Thumbnails on posts and pages.
			add_theme_support( 'post-thumbnails' );

			// Switch default core markup for search form, comment form, and comments.
			// to output valid HTML5.
			add_theme_support(
				'html5', array(
					'search-form',
		            'comment-form',
		            'comment-list',
		            'gallery',
		            'caption',
				)
			);

			// Post formats.
			add_theme_support(
				'post-formats', array(
					'gallery',
					'image',
					'link',
					'quote',
					'video',
					'audio',
					'status',
					'aside',
				)
			);

			// Add theme support for Custom Logo.
			add_theme_support(
				'custom-logo', array(
					'width'       => 100,
					'height'      => 400,
					'flex-width'  => true,
					'flex-height' => true,
				)
			);

			// Customize Selective Refresh Widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );

			// Add editor style
			// add_editor_style( 'assets/css/editor-style.min.css' );


			 // Register theme menu location
	        register_nav_menus( array(
	            'primary' => esc_html__( 'Primary Menu', 'laser' ),
	            'menu_footer' => esc_html__( 'Footer Menu', 'laser' )
	        ) );
		}


    /**
     * Register sidebars area.
     *
     * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
     */
    function register_sidebars() {
        register_sidebar( array(
            'name'          => esc_html__( 'Primary Sidebar', 'laser' ),
            'id'            => 'sidebar-1',
            'description'       => esc_html__( 'Add widgets here.', 'laser' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ) );
        register_sidebar( array(
            'name'          => esc_html__( 'Secondary Sidebar', 'laser' ),
            'id'            => 'sidebar-2',
            'description'       => esc_html__( 'Add widgets here.', 'laser' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ) );

        for( $i = 1; $i <= 6; $i++ ) {
            register_sidebar( array(
                /* translators: 1: Widget number. */
                'name'          => sprintf( __( 'Footer Sidebar %d', 'laser' ), $i ),
                'id'            => 'footer-'.$i,
                'description'       => __( 'Add widgets here.', 'laser' ),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h4 class="widget-title">',
                'after_title'   => '</h4>',
            ) );
        }

    }



	/**
     * Enqueue scripts and styles.
     */
    function enqueue_scripts() {

    $suffix = ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? '' : '.min';

	// Fitvids javscript library
	wp_enqueue_script( 'jquery.fitvids', LASER_THEME_URI . '/assets/js/vendors/jquery.fitvids.min.js', array('jquery'), LASER_THEME_VERSION, true );
	
	// Font Awesome css library
	if ( ! laser_is_disable_font_awesome() ) {
		
		// Remove font awesome style from plugins
		wp_deregister_style( 'font-awesome' );
		wp_deregister_style( 'fontawesome' );
		wp_enqueue_style( 'font-awesome', LASER_THEME_URI . '/assets/css/vendors/font-awesome.min.css', array(), '4.7.0', 'all' );	
		
	}	

	// Swiper css and js libraries
	if ( laser_get_banner_type() == 'swiper' ) {
		
		// Remove font awesome style from plugins
		wp_deregister_style( 'swiper' );
		wp_enqueue_style( 'swiper', LASER_THEME_URI . '/assets/css/vendors/swiper.min.css', array(), '4.3.3', 'all' );	
		
		wp_enqueue_script( 'swiper', LASER_THEME_URI . '/assets/js/vendors/swiper.min.js', array(), '4.3.3', true );
		
	}
		
	if ( $suffix == '') {
		// Javascript custom
		wp_enqueue_script( 'laser-navigation', LASER_THEME_URI . '/assets/js/custom/navigation.js', array(), LASER_THEME_VERSION, true );
		wp_enqueue_script( 'laser-skip-link-focus-fix', LASER_THEME_URI . '/assets/js/custom/skip-link-focus-fix.js', array(), LASER_THEME_VERSION, true );
		wp_enqueue_script( 'laser-back-to-top', LASER_THEME_URI . '/assets/js/custom/toTop.js', array('jquery'), LASER_THEME_VERSION, true );

		// Css custom
		wp_enqueue_style( 'laser-style', LASER_THEME_URI . '/assets/css/style.css', array(), LASER_THEME_VERSION, 'all' );	

	}else{

		// Javascript custom
		wp_enqueue_script( 'laser-script', LASER_THEME_URI . '/assets/js/script'. $suffix .'.js', array('jquery'), LASER_THEME_VERSION, true );

		// Css custom
		wp_enqueue_style( 'laser-style', LASER_THEME_URI . '/assets/css/style' .$suffix. '.css' );
	}

	// Comment reply script
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

	}

	 /**
	 * laser_custom_css hook
	 *
	 */
	public static function laser_custom_css( $output = NULL ) {
			    
	    // Add filter for adding custom css via other functions
		$output = apply_filters( 'laser_custom_css', $output );

			// Minify and output CSS in the wp_head
			if ( ! empty( $output ) ) {
				$css = ( defined( 'WP_DEBUG' ) && WP_DEBUG )? $output : laser_minify_css( $output );
				echo "<!-- Laser Custom CSS -->\n<style type=\"text/css\">\n" . wp_strip_all_tags( $css ) . "\n</style>";
			}

		}

	}
}
/**
 * Startup by calling 'get_instance()' method
 */
Laser_Theme_Setup::get_instance();
