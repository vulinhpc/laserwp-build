<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laser
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside itemtype="https://schema.org/WPSideBar" itemscope="itemscope" id="secondary" class="widget-area" role="complementary">

	<?php laser_sidebar_top(); ?>

		<?php dynamic_sidebar( 'sidebar-1' ); ?>
		
	<?php laser_sidebar_bottom(); ?>
	
</aside><!-- #secondary -->
