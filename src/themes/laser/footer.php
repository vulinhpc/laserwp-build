<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laser
 */

?>
		<?php laser_content_after(); ?>
		</div><!-- .laser-container -->
	</div><!-- #content -->
	<?php laser_content_bottom(); ?>

<?php laser_footer_before(); ?>
	<footer id="colophon" class="site-footer">
		<div class="laser-container">
	
			<div class="site-info">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'laser' ) ); ?>">
					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Proudly powered by %s', 'laser' ), 'WordPress' );
					?>
				</a>
				<span class="sep"> | </span>
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( 'Theme: %1$s by %2$s.', 'laser' ), 'laser', '<a href="http://laserwp.com">LaserWP</a>' );
					?>
			</div><!-- .site-info -->

		</div><!-- .laser-container -->
	</footer><!-- #colophon -->
<?php laser_footer_after(); ?>	

</div><!-- #page -->
<?php laser_body_bottom(); ?>

<?php laser_back_to_top(); ?>

<?php wp_footer(); ?>
</body>
</html>
