var $j 		= jQuery.noConflict(),
	$window = $j( window );

$j( document ).on( 'ready', function() {
	"use strict";
	// Back To top
	laserToTop();
} );

/* ==============================================
BACK TO TOP
============================================== */

function laserToTop(){

	var selectors = {
		backTotop: '#back-to-top',
		topLink: 'a[href="#back-to-top"]',
		topLinkWithSlash :  'a[href="/#back-to-top"]'
	}

	$j.each( selectors, function( key, value ){

			if ($j(value).length) {

		    var scrollTrigger = 100, // px
		        backToTop = function () {
		            var scrollTop = $j(window).scrollTop();
		            if (scrollTop > scrollTrigger) {
		                $j(value).addClass('show');
		            } else {
		                $j(value).removeClass('show');
		            }
		        };

		    backToTop();

		    $j(window).on('scroll', function () {
		        backToTop();
		    });

		    $j(value).on('click', function (e) {
		        e.preventDefault();
		        $j('html,body').animate({
		            scrollTop: 0
		        }, 500);
		    });

		}
	});

}