<?php
/**
 * Laser functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Laser
 */

/**
 * Define Constants
 */
define( 'LASER_THEME_VERSION', '1.0.0' );
define( 'LASER_THEME_SETTINGS', 'laser-settings' );
define( 'LASER_THEME_DIR', get_template_directory() );
define( 'LASER_THEME_URI', get_template_directory_uri() );

/**
 * Theme Hooks
 */
require LASER_THEME_DIR . '/inc/core/theme-hooks.php';
/**
 * Theme Setup
 */
require LASER_THEME_DIR . '/inc/core/class-laser-theme-setup.php';

/**
 * Implement the Custom Header feature.
 */
require LASER_THEME_DIR . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require LASER_THEME_DIR . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require LASER_THEME_DIR . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require LASER_THEME_DIR . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require LASER_THEME_DIR . '/inc/compatibility/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/compatibility/woocommerce.php';
}
