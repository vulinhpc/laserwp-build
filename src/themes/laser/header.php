<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package laser
 */
?>
<!doctype html>
<?php laser_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php laser_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
<?php laser_head_bottom(); ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php laser_body_top(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'laser' ); ?></a>
	
	<?php laser_header_before(); ?>
	<header id="masthead" class="site-header">
		<div class="laser-container">
			<div class="laser-row">
				<div class="laser-col-12">
					<div class="site-branding">
						<?php
						the_custom_logo();
						if ( is_front_page() && is_home() ) :
							?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php
						else :
							?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php
						endif;
						$laser_description = get_bloginfo( 'description', 'display' );
						if ( $laser_description || is_customize_preview() ) :
							?>
							<p class="site-description"><?php echo $laser_description; /* WPCS: xss ok. */ ?></p>
						<?php endif; ?>
					</div><!-- .site-branding -->

					<nav id="site-navigation" class="main-navigation">
						<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'laser' ); ?></button>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
						?>
					</nav><!-- #site-navigation -->
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<?php laser_header_after(); ?>

	<?php laser_banner_before(); ?>
	<?php laser_banner_section(); ?>
	<?php laser_banner_after(); ?>

	<?php laser_content_before(); ?>
	<div id="content" class="site-content">
		<div class="laser-container">
		<?php laser_content_top(); ?>