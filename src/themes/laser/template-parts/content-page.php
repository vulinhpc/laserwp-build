<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package laser
 */

?>

<?php laser_entry_before(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php laser_entry_top(); ?>

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->

		<?php laser_post_thumbnail(); ?>

		<div class="entry-content">

			<?php laser_entry_content_before() ?>

			<?php

			the_content();

			?>

			<?php laser_entry_content_after() ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'laser' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

		<?php if ( get_edit_post_link() ) : ?>
			<footer class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'laser' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</footer><!-- .entry-footer -->
		<?php endif; ?>

		<?php laser_entry_bottom(); ?>

	</article><!-- #post-<?php the_ID(); ?> -->

<?php laser_entry_after(); ?>
