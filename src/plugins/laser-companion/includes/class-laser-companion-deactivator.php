<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://laserwp.com
 * @since      1.0.0
 *
 * @package    Laser_Companion
 * @subpackage Laser_Companion/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Laser_Companion
 * @subpackage Laser_Companion/includes
 * @author     LaserWP <hello@laserwp.com>
 */
class Laser_Companion_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
