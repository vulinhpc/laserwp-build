<?php

/**
 * Fired during plugin activation
 *
 * @link       http://laserwp.com
 * @since      1.0.0
 *
 * @package    Laser_Companion
 * @subpackage Laser_Companion/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Laser_Companion
 * @subpackage Laser_Companion/includes
 * @author     LaserWP <hello@laserwp.com>
 */
class Laser_Companion_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
